const path = require("path");

module.exports = {
  // async redirects() {
  //   return [
  //     {
  //       source: "/",
  //       destination: "/th/home", // Matched parameters can be used in the destination
  //       permanent: false,
  //     },
  //   ];
  // },
  trailingSlash: true,
  async redirects() {
    return [
      {
        source: "/",
        destination: "/th/home", // Matched parameters can be used in the destination
        permanent: false,
      },
    ];
  },
  env: {
    // api_url: "http://localhost:1337",
    api_url: "http://civilmasterdecor.co.th:1337",
  },
  target: "serverless",
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.node = {
        fs: "empty",
      };
    }

    config.resolve.alias["components"] = path.join(__dirname, "components");
    config.resolve.alias["public"] = path.join(__dirname, "public");

    return config;
  },
};
