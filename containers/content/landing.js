import React from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
export const Landing = () => {
  return (
    <Section width="100%" bg="/assets/home-bg.png">
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          minHeight="60vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          >
            <Text.H2
              weight={400}
              space="2.5px"
              margin="1em 0 0 0"
              fontColor="white"
              textAlign="center"
            >
              Contents, Events and News
            </Text.H2>
            <Text.H5
              weight={200}
              space="2.5px"
              margin="0 0 1em 0"
              fontColor="white"
              textAlign="center"
            >
              CIVIL MASTER DECOR ● The Trademark of Quality and Service
            </Text.H5>
          </Flex>
          <DivWrapper margin="2em 0">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
