import React from "react";
import { Section, Flex, Text } from "components";
import { Container } from "reactstrap";
export const Intro = ({ data }) => {
  return (
    <Section padding="5em 0">
      <Container>
        <Flex>
          <Flex
            className="responsive-hide "
            width="50%"
            bg={data && `${process.env.api_url}` + data.introImg.url}
          ></Flex>
          <Flex
            className="responsive-full-width "
            width="50%"
            padding="2em"
            flexDirection="column"
          >
            <Text.H3 weight={500} fontColor="red">
              {data && data.introHeaderTh}
            </Text.H3>
            <Text.H5
              margin="2em 0 0 0"
              fontColor="white"
              space="2px"
              lineHeight="25px"
              weight={200}
            >
              {data && data.introTextTh}
            </Text.H5>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
