import React, { useState } from "react";
import { Section, Text, Flex, ContactForm } from "components";
import { Container, Spinner } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhoneAlt,
  faEnvelope,
  faMapMarkedAlt,
} from "@fortawesome/free-solid-svg-icons";

import axios from "axios";
export const VissionAndMission = ({ data }) => {
  const [onLoading, setLoading] = useState(false);
  const [onSubmitted, setSubmitted] = useState(false);
  // const defaultOptions = {
  //   loop: true,
  //   autoplay: true,
  //   animationData: animationData,
  //   rendererSettings: {
  //     preserveAspectRatio: "xMidYMid slice",
  //   },
  // };
  const handleSubmit = (e) => {
    setLoading(true);
    const fd = new FormData();

    console.log("submiitng: " + e.name);
    axios
      .post(`${process.env.api_url}/email-inboxes`, {
        name: e.name,
        company: e.company,
        email: e.email,
        telephone: e.telephone,
        message: e.message,
      })
      .then(function (response) {
        console.log(response);
        setSubmitted(true);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  return (
    <Section padding="2em 0 5em 0">
      <Container>
        <Text.H3 margin="1em 0 2em 0" weight={500} fontColor="red">
          วิสัยทัศน์
        </Text.H3>
        <Text.H5
          margin="0 0 0 3em"
          fontColor="white"
          weight={300}
          space="1.5px"
          lineHeight="25px"
        >
          {data && data.vissionTh}
        </Text.H5>
        <Text.H3 margin="2em 0 1em 0" weight={500} fontColor="red">
          ภารกิจ
        </Text.H3>
        <Text.H5
          margin="0 0 0 3em"
          fontColor="white"
          weight={300}
          space="1.5px"
          lineHeight="25px"
        >
          {data && data.missionTh}
        </Text.H5>
        <Flex flexDirection="column" justifyContent="center"></Flex>
        <Text.H3
          textAlign="center"
          margin="2em 0 1em 0"
          weight={500}
          fontColor="red"
        >
          ติดต่อเรา
        </Text.H3>
        <Flex alignItems="center" justifyContent="center">
          <FontAwesomeIcon
            icon={faMapMarkedAlt}
            style={{ width: "25px", color: "white" }}
          />
          <Text.H5
            fontColor="white"
            textAlign="center"
            weight={300}
            space="1.5px"
            lineHeight="25px"
            margin="0.5em"
          >
            {data && data.addressTh}
          </Text.H5>
        </Flex>
        <Flex alignItems="center" justifyContent="center">
          <FontAwesomeIcon
            icon={faPhoneAlt}
            style={{ width: "25px", color: "white" }}
          />
          <Text.H5
            fontColor="white"
            textAlign="center"
            weight={300}
            space="1.5px"
            lineHeight="25px"
            margin="0.5em"
          >
            {data && data.phone}
          </Text.H5>
        </Flex>
        <Flex alignItems="center" justifyContent="center">
          <FontAwesomeIcon
            icon={faEnvelope}
            style={{ width: "25px", color: "white" }}
          />
          <Text.H5
            fontColor="white"
            textAlign="center"
            weight={300}
            space="1.5px"
            lineHeight="25px"
            margin="0.5em"
          >
            {data && data.email}
          </Text.H5>
        </Flex>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <Text.H3
            textAlign="center"
            margin="1em 0"
            weight={500}
            fontColor="red"
          >
            Our Line qr code
          </Text.H3>
          <img src="/assets/line-cmd.jpg" style={{ maxWidth: "300px" }} />
        </Flex>
        <Flex className="responsive-column ">
          <Flex
            className="responsive-full-width "
            width="50%"
            margin="5em 0 0 0"
          >
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15503.669645942799!2d100.4752651!3d13.7234498!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x85006e72919c8936!2sCivil%20Master%20Group%20Company%20Limited!5e0!3m2!1sen!2sth!4v1626911161430!5m2!1sen!2sth"
              style={{ border: "0", width: "100%" }}
              height="450"
              loading="lazy"
            />
          </Flex>
          <Flex
            className="responsive-full-width "
            width="50%"
            margin="5em 0 0 0"
            justifyContent="center"
            alignItems="center"
            padding="0 1em"
          >
            {/* <Lottie options={defaultOptions} style={{ width: "100%" }} /> */}
            {onSubmitted ? (
              <Text.H4 fontColor="red" textAlign="center">
                Already Submitted
                <br />
                <br /> Thank you we will get back to you as soon as possible.
              </Text.H4>
            ) : onLoading ? (
              <Spinner color="danger" />
            ) : (
              <ContactForm handleSubmit={handleSubmit} />
            )}
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
