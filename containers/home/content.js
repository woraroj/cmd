import React, { useState } from "react";
import { Section, Flex, Text, Button } from "components";
import { Container, Spinner } from "reactstrap";
import Link from "next/link";
export const Content = ({ contentData }) => {
  return (
    <Section padding="2em">
      <Container>
        <Flex justifyContent="center" flexDirection="column">
          <Text.H3 margin="2em 0" fontColor="red" textAlign="left">
            Contents News and Events
          </Text.H3>
          <Flex width="100%" wrap="wrap" className="responsive-column ">
            {contentData.map((data, index) => (
              <ContentWrapper key={index} con={data} />
            ))}
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};

const ContentWrapper = ({ con }) => {
  const [hover, setHover] = useState(false);

  return (
    <Flex
      className="responsive-full-width responsive-height250px filter"
      bg={con && `${process.env.api_url}` + con.image[0].url}
      flexDirection="column"
      width="33.33%"
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H3 textAlign="center" fontColor="white" weight={hover ? 500 : 100}>
        {con.name}
      </Text.H3>
      <Link href={`/th/content/content?name=${con.url}`}>
        <a>
          <Button
            margin="0.5em 0"
            bgcolor="red"
            className={hover ? "inline visible-show" : "inline visible-hide"}
          >
            View now
          </Button>
        </a>
      </Link>
    </Flex>
  );
};
