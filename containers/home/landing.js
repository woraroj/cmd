import React from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Landing = ({ bg }) => {
  return (
    <Section width="100%" bg={bg && `${process.env.api_url}` + bg.bannerBg.url}>
      <Container>
        <Flex
          className="responsive-align-center "
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-start"
          height="100vh"
        >
          <Text.H2
            weight={400}
            space="2.5px"
            textAlign="center"
            fontColor="white"
          >
            CIVIL MASTER DECOR
          </Text.H2>
          <Text.H5
            fontColor="white"
            weight={300}
            textAlign="center"
            margin="1em 0 1.5em 0"
          >
            The Trademark of Quality and Service
          </Text.H5>
          <Link href="/th/service">
            <a>
              <Button bgcolor="red">Our Project →</Button>
            </a>
          </Link>
          <DivWrapper margin="2em 0">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
