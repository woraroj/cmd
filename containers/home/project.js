import React, { useState } from "react";
import { Section, Flex, Text, Button } from "components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Project = ({ data }) => {
  const [hover, setHover] = useState({
    pro1: false,
    pro2: false,
    pro3: false,
    pro4: false,
  });
  return (
    <Section padding="2em">
      <Container>
        <Flex justifyContent="center" flexDirection="column">
          <Text.H3 margin="2em 0" fontColor="red" textAlign="center">
            Our Project
          </Text.H3>
          <Flex className="responsive-column " wrap="wrap">
            <Flex
              className="responsive-full-width responsive-height250px filter"
              bg={data && `${process.env.api_url}` + data.project1Img.url}
              flexDirection="column"
              width="50%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ pro1: true })}
              onMouseLeave={() => setHover({ pro1: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.pro1 === true ? 500 : 100}
              >
                {data.project1Name}
              </Text.H3>
              <Link href={`/th/service/project?project=${data.project1Link}`}>
                <a>
                  <Button
                    margin="0.5em 0"
                    bgcolor="red"
                    className={
                      hover.pro1 === true
                        ? "inline visible-show"
                        : "inline visible-hide"
                    }
                  >
                    View now
                  </Button>
                </a>
              </Link>
            </Flex>
            <Flex
              bg={data && `${process.env.api_url}` + data.project2Img.url}
              className="responsive-full-width responsive-height250px filter"
              flexDirection="column"
              width="50%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ pro2: true })}
              onMouseLeave={() => setHover({ pro2: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.pro2 === true ? 500 : 100}
              >
                {data.project2Name}
              </Text.H3>
              <Link href={`/th/service/project?project=${data.project2Link}`}>
                <a>
                  <Button
                    margin="0.5em 0"
                    bgcolor="red"
                    className={
                      hover.pro2 === true
                        ? "inline visible-show"
                        : "inline visible-hide"
                    }
                  >
                    View now
                  </Button>
                </a>
              </Link>
            </Flex>
            <Flex
              bg={data && `${process.env.api_url}` + data.project3Img.url}
              className="responsive-full-width responsive-height250px filter"
              flexDirection="column"
              width="50%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ pro3: true })}
              onMouseLeave={() => setHover({ pro3: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.pro3 === true ? 500 : 100}
              >
                {data.project3Name}
              </Text.H3>
              <Link href={`/th/service/project?project=${data.project3Link}`}>
                <a>
                  <Button
                    margin="0.5em 0"
                    bgcolor="red"
                    className={
                      hover.pro3 === true
                        ? "inline visible-show"
                        : "inline visible-hide"
                    }
                  >
                    View now
                  </Button>
                </a>
              </Link>
            </Flex>
            <Flex
              bg="/assets/serv2.png"
              className="responsive-full-width responsive-height250px"
              flexDirection="column"
              width="50%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ pro4: true })}
              onMouseLeave={() => setHover({ pro4: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.pro4 === true ? 500 : 100}
              >
                View more →
              </Text.H3>
              <Link href="/th/service">
                <a>
                  <Button
                    margin="0.5em 0"
                    bgcolor="red"
                    className={
                      hover.pro4 === true
                        ? "inline visible-show"
                        : "inline visible-hide"
                    }
                  >
                    View now
                  </Button>
                </a>
              </Link>
            </Flex>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
