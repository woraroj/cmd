import React, { useState } from "react";
import { Section, Flex, Text, Button } from "components";
import { Container } from "reactstrap";
import { useRouter } from "next/router";
export const Service = () => {
  const [hover, setHover] = useState({
    serv1: false,
    serv2: false,
    serv3: false,
    serv4: false,
  });
  const router = useRouter();
  return (
    <Section padding="2em">
      <Container>
        <Flex justifyContent="center" flexDirection="column">
          <Text.H3 margin="2em 0" fontColor="red" textAlign="left">
            Our Services
          </Text.H3>
          <Flex className="responsive-column ">
            <Flex
              className="responsive-full-width responsive-height250px"
              bg="/assets/serv1.png"
              flexDirection="column"
              width="33%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ serv1: true })}
              onMouseLeave={() => setHover({ serv1: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.serv1 === true ? 500 : 100}
              >
                Construction
              </Text.H3>
              <Button
                margin="0.5em 0"
                bgcolor="red"
                className={
                  hover.serv1 === true
                    ? "inline visible-show"
                    : "inline visible-hide"
                }
                onClick={() =>
                  router.push("/th/service/type?type=construction")
                }
              >
                View now
              </Button>
            </Flex>
            <Flex
              bg="/assets/serv2.png"
              className="responsive-full-width responsive-height250px"
              flexDirection="column"
              width="33%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ serv2: true })}
              onMouseLeave={() => setHover({ serv2: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.serv2 === true ? 500 : 100}
              >
                Interior
              </Text.H3>
              <Button
                margin="0.5em 0"
                bgcolor="red"
                className={
                  hover.serv2 === true
                    ? "inline visible-show"
                    : "inline visible-hide"
                }
                onClick={() => router.push("/th/service/type?type=interior")}
              >
                View now
              </Button>
            </Flex>
            <Flex
              bg="/assets/serv3.png"
              className="responsive-full-width responsive-height250px"
              flexDirection="column"
              width="33%"
              minHeight="300px"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              onMouseOver={() => setHover({ serv3: true })}
              onMouseLeave={() => setHover({ serv3: false })}
            >
              <Text.H3
                fontColor="white"
                weight={hover.serv3 === true ? 500 : 100}
              >
                Acoustic
              </Text.H3>
              <Button
                margin="0.5em 0"
                bgcolor="red"
                className={
                  hover.serv3 === true
                    ? "inline visible-show"
                    : "inline visible-hide"
                }
                onClick={() => router.push("/th/service/type?type=acoustic")}
              >
                View now
              </Button>
            </Flex>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
