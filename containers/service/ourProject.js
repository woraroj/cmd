import React, { useState } from "react";
import { Section, Flex, Text, Button } from "components";
import { Container } from "reactstrap";
import Link from "next/link";
export const OurProject = ({ project }) => {
  const construction =
    project &&
    project
      .filter((project) => project.type === "construction")
      .sort((a, b) => a.index - b.index);
  const interior =
    project &&
    project
      .filter((project) => project.type === "interior")
      .sort((a, b) => a.index - b.index);
  const acoustic =
    project &&
    project
      .filter((project) => project.type === "acoustic")
      .sort((a, b) => a.index - b.index);
  // const under_construction =
  //   project &&
  //   project.filter((project) => project.type === "under construction");

  return (
    <Section padding="2em">
      <Container>
        <Flex justifyContent="center" flexDirection="column">
          <Text.H3 margin="2em 0 1em 0" fontColor="red" textAlign="left">
            Our Project
          </Text.H3>
          <Text.H3 margin="1em 0" fontColor="white" textAlign="right">
            Construction
          </Text.H3>
          {project && (
            <>
              <Project data={construction} />
              <Text.H3 margin="1em 0" fontColor="white" textAlign="right">
                Interior
              </Text.H3>
              <Project data={interior} />
              <Text.H3 margin="1em 0" fontColor="white" textAlign="right">
                Acoustic
              </Text.H3>
              <Project data={acoustic} />
            </>
          )}
        </Flex>
      </Container>
    </Section>
  );
};

const Project = ({ data }) => {
  const [hover, setHover] = useState({
    pro1: false,
    pro2: false,
    pro3: false,
    pro4: false,
  });
  return (
    <Flex className="responsive-column ">
      <Flex
        className="responsive-full-width responsive-height250px filter"
        bg={data && `${process.env.api_url}` + data[0].image[0].url}
        flexDirection="column"
        width="25%"
        minHeight="300px"
        justifyContent="center"
        alignItems="center"
        cursor="pointer"
        onMouseOver={() => setHover({ pro1: true })}
        onMouseLeave={() => setHover({ pro1: false })}
      >
        <Text.H3
          fontColor="white"
          textAlign="center"
          weight={hover.pro1 === true ? 500 : 100}
        >
          {data[0].name}
        </Text.H3>
        <Link href={`/th/service/project?project=${data[0].url}`}>
          <a>
            <Button
              margin="0.5em 0"
              bgcolor="red"
              className={
                hover.pro1 === true
                  ? "inline visible-show"
                  : "inline visible-hide"
              }
            >
              View now
            </Button>
          </a>
        </Link>
      </Flex>
      <Flex
        bg={data && `${process.env.api_url}` + data[1].image[1].url}
        className="responsive-full-width responsive-height250px filter"
        flexDirection="column"
        width="25%"
        minHeight="300px"
        justifyContent="center"
        alignItems="center"
        cursor="pointer"
        onMouseOver={() => setHover({ pro2: true })}
        onMouseLeave={() => setHover({ pro2: false })}
      >
        <Text.H3
          fontColor="white"
          textAlign="center"
          weight={hover.pro2 === true ? 500 : 100}
        >
          {data[1].name}
        </Text.H3>
        <Link href={`/th/service/project?project=${data[1].url}`}>
          <a>
            <Button
              margin="0.5em 0"
              bgcolor="red"
              className={
                hover.pro2 === true
                  ? "inline visible-show"
                  : "inline visible-hide"
              }
            >
              View now
            </Button>
          </a>
        </Link>
      </Flex>
      <Flex
        bg={data && `${process.env.api_url}` + data[2].image[2].url}
        className="responsive-full-width responsive-height250px filter"
        flexDirection="column"
        width="25%"
        minHeight="300px"
        justifyContent="center"
        alignItems="center"
        cursor="pointer"
        onMouseOver={() => setHover({ pro3: true })}
        onMouseLeave={() => setHover({ pro3: false })}
      >
        <Text.H3
          fontColor="white"
          textAlign="center"
          weight={hover.pro3 === true ? 500 : 100}
        >
          {data[2].name}
        </Text.H3>
        <Link href={`/th/service/project?project=${data[2].url}`}>
          <a>
            <Button
              margin="0.5em 0"
              bgcolor="red"
              className={
                hover.pro3 === true
                  ? "inline visible-show"
                  : "inline visible-hide"
              }
            >
              View now
            </Button>
          </a>
        </Link>
      </Flex>
      <Flex
        bg={data && `${process.env.api_url}` + data[3].image[3].url}
        className="responsive-full-width responsive-height250px filter"
        flexDirection="column"
        width="25%"
        minHeight="300px"
        justifyContent="center"
        alignItems="center"
        cursor="pointer"
        onMouseOver={() => setHover({ pro4: true })}
        onMouseLeave={() => setHover({ pro4: false })}
      >
        <Text.H3
          fontColor="white"
          textAlign="center"
          weight={hover.pro4 === true ? 500 : 100}
        >
          {data[3].name}
        </Text.H3>
        <Link href={`/th/service/project?project=${data[3].url}`}>
          <a>
            <Button
              margin="0.5em 0"
              bgcolor="red"
              className={
                hover.pro4 === true
                  ? "inline visible-show"
                  : "inline visible-hide"
              }
            >
              View now
            </Button>
          </a>
        </Link>
      </Flex>
    </Flex>
  );
};
