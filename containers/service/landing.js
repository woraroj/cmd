import React from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
import useSWR from "swr";
import fetch from "unfetch";
export const Landing = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data, dataError } = useSWR(`${process.env.api_url}/aboutus`, fetcher);
  return (
    <Section
      width="100%"
      bg={data && `${process.env.api_url}` + data.bannerBg.url}
    >
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          height="60vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          >
            <Text.H1
              weight={400}
              space="2.5px"
              margin="1em 0 0 0"
              fontColor="white"
              textAlign="center"
            >
              Our Services
            </Text.H1>
            <Text.H5
              weight={200}
              space="2.5px"
              margin="0 0 1em 0"
              fontColor="white"
              textAlign="center"
            >
              CIVIL MASTER DECOR ● The Trademark of Quality and Service
            </Text.H5>
          </Flex>
          <DivWrapper margin="2em 0">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
