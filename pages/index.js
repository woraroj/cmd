import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import { useRouter } from "next/router";
import { motion } from "framer-motion";
const Home = () => {
  const router = useRouter();
  useEffect(() => {
    router.push("/th/home");
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section height="100vh">
        <Head>
          <title>Loading... • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
      </Section>
    </motion.div>
  );
};
export default Home;
