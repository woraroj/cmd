import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.css";
import { ThemeProvider } from "styled-components";
import theme from "../themes";
import { AnimatePresence } from "framer-motion";
import { Header, Footer } from "components";
import "slick-carousel/slick/slick.css";

import "slick-carousel/slick/slick-theme.css";
function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <AnimatePresence exitBeforeEnter>
        <Component {...pageProps} />
      </AnimatePresence>
      <Footer />
    </ThemeProvider>
  );
}

export default MyApp;
