import React, { useEffect } from "react";
import { Section, Flex } from "components";
import { Spinner } from "reactstrap";
import Head from "next/head";
import { motion } from "framer-motion";
import { Landing, OurService, OurProject } from "../../../containers/service";
import useSWR from "swr";
import fetch from "unfetch";
const Service = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data, projectError } = useSWR(
    `${process.env.api_url}/services`,
    fetcher
  );
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="100vh">
        <Head>
          <title>บริการ • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing />
        <OurService />
        {data ? (
          <OurProject project={data} />
        ) : (
          <Flex
            width="100%"
            height="500px"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};
export default Service;
