import React, { useEffect } from "react";
import Head from "next/head";
import { Section, Flex } from "components";
import { Spinner } from "reactstrap";
import useSWR from "swr";
import fetch from "unfetch";
import { motion } from "framer-motion";
import { Landing, Intro, VissionAndMission } from "../../../containers/aboutus";
const Aboutus = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data, error } = useSWR(`${process.env.api_url}/aboutus`, fetcher);
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>เกี่ยวกับเรา • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {data ? (
          <>
            <Landing bg={data && data.bannerBg} />
            <Intro data={data} />
            <VissionAndMission data={data} />
          </>
        ) : (
          <Flex
            width="100%"
            height="80vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};
export default Aboutus;
