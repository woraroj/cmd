import React, { useEffect } from "react";
import Head from "next/head";
import { Section, Flex } from "components";
import { Spinner } from "reactstrap";
import useSWR from "swr";
import fetch from "unfetch";
import { Landing, Service, Project, Content } from "../../../containers";
import { motion } from "framer-motion";
const Home = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: homeData, error: homeError } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );

  const { data: contentData, error: contentsError } = useSWR(
    `${process.env.api_url}/contents`,
    fetcher
  );
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section bgcolor="black">
        <Head>
          <title>หน้าแรก • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {homeData && contentData ? (
          <>
            <Landing bg={homeData} />
            <Service />
            <Project data={homeData} />
            <Content contentData={contentData} />
          </>
        ) : (
          <Flex
            width="100%"
            height="80vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};
export default Home;
