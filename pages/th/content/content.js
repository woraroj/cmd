import React, { useState, useEffect } from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container, Spinner } from "reactstrap";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import Head from "next/head";
import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import Slider from "react-slick";
import Custom404 from "../../404";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactMarkdown from "react-markdown";
const Content = () => {
  const router = useRouter();

  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data: contentData, error: typeDataError } = useSWR(
    `${process.env.api_url}/contents?url=${router.query.name}`,
    fetcher
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [router]);

  if (Array.isArray(contentData) && contentData.length === 0) {
    return <Custom404 />;
  }
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="100vh">
        <Head>
          <title>บริการ • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {contentData ? (
          <>
            <Landing contentData={contentData[0]} />
            <Main contentData={contentData[0]} />
          </>
        ) : (
          <Flex
            width="100%"
            height="50vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};

const Landing = ({ contentData }) => {
  return (
    <Section
      className="filter"
      width="100%"
      bg={contentData && `${process.env.api_url}` + contentData.image[0].url}
    >
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          height="60vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
            textAlign="center"
          >
            <Text.H1
              weight={400}
              space="2.5px"
              margin="1em 0 0 0"
              fontColor="white"
              textAlign="center"
            >
              {contentData && contentData.name}
            </Text.H1>
            <Text.H5
              weight={200}
              space="2.5px"
              margin="0 0 1em 0"
              fontColor="white"
            >
              CIVIL MASTER DECOR ● The Trademark of Quality and Service
            </Text.H5>
          </Flex>
          <DivWrapper margin="2em 0">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

const Main = ({ contentData }) => {
  const settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: false,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: false,
          adaptiveHeight: false,
        },
      },
      {
        breakpoint: 992,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          adaptiveHeight: false,
          variableWidth: false,
        },
      },
    ],
    className: "center",
    focusOnSelect: true,
    centerPadding: "60px",
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    mobileFirst: true,
    slidesToScroll: 1,
    variableWidth: true,
    adaptiveHeight: false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="white"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="white"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  return (
    <Container>
      <Flex flexDirection="column" margin="1em 0">
        <Text.H2
          weight={400}
          space="2.5px"
          margin="1em 0 0 0"
          fontColor="white"
          textAlign="center"
        >
          {contentData && contentData.name}
        </Text.H2>
        <Slider {...settings}>
          {contentData.image.map((data, index) => (
            <DivWrapper className="client-image " key={index}>
              <img
                src={`${process.env.api_url}` + data.url}
                style={{ maxHeight: "480px", maxWidth: "95%" }}
              />
            </DivWrapper>
          ))}
        </Slider>
        <DivWrapper margin="3em 0 0 0">
          <ReactMarkdown className="article">{contentData.text}</ReactMarkdown>
        </DivWrapper>
      </Flex>
    </Container>
  );
};

export default Content;
