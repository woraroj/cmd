import React, { useState } from "react";
import { Section, Flex, Text, Button } from "components";
import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";
import { Landing } from "../../../containers/content";
import { Spinner, Container } from "reactstrap";
import useSWR from "swr";
import fetch from "unfetch";

const Service = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data, error } = useSWR(`${process.env.api_url}/contents`, fetcher);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="100vh">
        <Head>
          <title>ข่าวสาร • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing />
        {data ? (
          <MainContent cen={data} />
        ) : (
          <Flex
            width="100%"
            height="50vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};

const MainContent = ({ cen }) => {
  console.log(cen.filter((allcen) => allcen.type === "news"));
  return (
    <Container>
      <Flex flexDirection="column" padding="3em 0">
        <Text.H3 margin="2em 0 1em 0" fontColor="red">
          CONTENTS
        </Text.H3>
        <div className="grid1">
          {cen.filter((allcen) => allcen.type === "content").length === 0 ? (
            <Text.H4 fontColor="white">NO CONTENT AVAILABLE</Text.H4>
          ) : (
            cen
              .filter((allcen) => allcen.type === "content")
              .map((data, index) => <ContentWrapper key={index} con={data} />)
          )}
        </div>

        <Text.H3 margin="2em 0 1em 0" fontColor="red">
          EVENTS
        </Text.H3>
        <div className="grid1">
          {cen.filter((allcen) => allcen.type === "event").length === 0 ? (
            <Text.H4 fontColor="white">NO EVENT AVAILABLE</Text.H4>
          ) : (
            cen
              .filter((allcen) => allcen.type === "event")
              .map((data, index) => <ContentWrapper key={index} con={data} />)
          )}
        </div>
        <Text.H3 margin="2em 0 1em 0" fontColor="red">
          NEWS
        </Text.H3>
        <div className="grid1">
          {cen.filter((allcen) => allcen.type === "news").length === 0 ? (
            <Text.H4 fontColor="white">NO NEWS AVAILABLE</Text.H4>
          ) : (
            cen
              .filter((allcen) => allcen.type === "news")
              .map((data, index) => <ContentWrapper key={index} con={data} />)
          )}
        </div>
      </Flex>
    </Container>
  );
};

const ContentWrapper = ({ con }) => {
  const [hover, setHover] = useState(false);

  return (
    <Flex
      className="responsive-full-width responsive-height250px filter"
      bg={con && `${process.env.api_url}` + con.image[0].url}
      flexDirection="column"
      width="100%"
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H3 textAlign="center" fontColor="white" weight={hover ? 500 : 100}>
        {con.name}
      </Text.H3>
      <Link href={`/th/content/content?name=${con.url}`}>
        <a>
          <Button
            margin="0.5em 0"
            bgcolor="red"
            className={hover ? "inline visible-show" : "inline visible-hide"}
          >
            View now
          </Button>
        </a>
      </Link>
    </Flex>
  );
};
export default Service;
