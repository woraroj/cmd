import React from "react";
import { Section, Flex, Text } from "components";
import { Container } from "reactstrap";
import { motion } from "framer-motion";
import Head from "next/head";
const Custom404 = () => {
  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" padding="0 0 5em 0">
        <Head>
          <title>404 • CIVIL MASTER DECOR</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Container>
          <Flex justifyContent="center" alignItems="center" height="80vh">
            <Text.H2 fontColor="red" textAlign="center">
              404 NOT FOUND
            </Text.H2>
          </Flex>
        </Container>
      </Section>
    </motion.div>
  );
};
export default Custom404;
