export default {
  white: "#FFFFFF",
  black: "#111111",
  blue: "#3468C4",
  red: "#FF3300",
};
