import React, { useEffect, useState } from "react";
import { Section, Flex, DivWrapper, Text, SocialBar } from "components";
import { Container, Spinner } from "reactstrap";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import Head from "next/head";
import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import Custom404 from "../../404";
import Slider from "react-slick";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const ProjectDetail = () => {
  const router = useRouter();
  const [slug, setSlug] = useState();
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data: productData, error: typeDataError } = useSWR(
    `${process.env.api_url}/services?url=${slug}`,
    fetcher
  );

  function capitalizeLetter(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    if (router && router.query.project) {
      const param = router.query.project[1];
      setSlug(param);
    }
  }, [router]);
  const settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: false,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: false,
          adaptiveHeight: false,
        },
      },
      {
        breakpoint: 992,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          adaptiveHeight: false,
          variableWidth: false,
        },
      },
    ],
    className: "center",
    focusOnSelect: true,
    centerPadding: "60px",
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    mobileFirst: true,
    slidesToScroll: 1,
    variableWidth: true,
    adaptiveHeight: false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="white"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="white"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  if (Array.isArray(productData) && productData.length === 0) {
    return <Custom404 />;
  }
  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" padding="0 0 5em 0">
        <Head>
          <title>
            {productData && productData.length > 0
              ? `${capitalizeLetter(productData[0].name)} • CIVIL MASTER DECOR`
              : "Loading..."}
          </title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {productData ? (
          <>
            <Landing
              productData={productData}
              capitalizeLetter={capitalizeLetter}
            />
            <Container>
              <DivWrapper margin="5em 0 0 0">
                {productData && (
                  <Slider {...settings}>
                    {productData[0].image.map((data, index) => (
                      <DivWrapper className="client-image " key={index}>
                        <img
                          src={`${process.env.api_url}` + data.url}
                          style={{ maxHeight: "480px", maxWidth: "95%" }}
                        />
                      </DivWrapper>
                    ))}
                  </Slider>
                )}
              </DivWrapper>
            </Container>
          </>
        ) : (
          <Flex
            width="100%"
            height="80vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};

const Landing = ({ productData, capitalizeLetter }) => {
  return (
    <Section
      width="100%"
      bg={productData && `${process.env.api_url}` + productData[0].image[0].url}
      className="filter"
    >
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          minHeight="60vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          >
            <Text.H3
              weight={400}
              space="2.5px"
              margin="1em 0 0 0"
              fontColor="white"
              textAlign="center"
            >
              {productData &&
                productData.length > 0 &&
                `${capitalizeLetter(productData[0].name)}`}
            </Text.H3>
            <Text.H5
              weight={200}
              space="2.5px"
              margin="1em 0 1em 0"
              fontColor="white"
              textAlign="center"
            >
              CIVIL MASTER DECOR ● The Trademark of Quality and Service
            </Text.H5>
          </Flex>

          <DivWrapper>
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

export default ProjectDetail;
