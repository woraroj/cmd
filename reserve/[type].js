import React, { useEffect, useState } from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container, Spinner } from "reactstrap";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import Head from "next/head";
import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import Custom404 from "../../404";
import Link from "next/link";
const ServiceType = () => {
  const router = useRouter();
  const { type } = router.query;
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: landing, error: landingError } = useSWR(
    `${process.env.api_url}/aboutus`,
    fetcher
  );
  const { data: typeData, error: typeDataError } = useSWR(
    `${process.env.api_url}/services?type=${router.query.type}`,
    fetcher
  );

  function capitalizeLetter(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  if (Array.isArray(typeData) && typeData.length === 0) {
    return <Custom404 />;
  }
  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" padding="0 0 5em 0">
        <Head>
          <title>
            {typeData && typeData.length > 0
              ? `${capitalizeLetter(typeData[0].type)} • CIVIL MASTER DECOR`
              : "Loading..."}
          </title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {typeData ? (
          <>
            <Landing
              typeData={typeData}
              landing={landing}
              capitalizeLetter={capitalizeLetter}
            />
            <Container>
              <Text.H3
                weight={400}
                space="2.5px"
                margin="2em 0"
                fontColor="white"
                textAlign="center"
              >
                {typeData &&
                  typeData.length > 0 &&
                  `${capitalizeLetter(typeData[0].type)}`}
              </Text.H3>
              <Flex wrap="wrap">
                {typeData &&
                  typeData.map((data, index) => <BoxContent data={data} />)}
              </Flex>
            </Container>
          </>
        ) : (
          <Flex
            width="100%"
            height="80vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};

const Landing = ({ typeData, landing, capitalizeLetter }) => {
  return (
    <Section
      width="100%"
      bg={landing && `${process.env.api_url}` + landing.bannerBg.url}
    >
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          height="60vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          >
            <Text.H1
              weight={400}
              space="2.5px"
              margin="1em 0 0 0"
              fontColor="white"
            >
              {typeData &&
                typeData.length > 0 &&
                `${capitalizeLetter(typeData[0].type)}`}
            </Text.H1>
            <Text.H5
              weight={200}
              space="2.5px"
              margin="0 0 1em 0"
              fontColor="white"
            >
              CIVIL MASTER DECOR ● The Trademark of Quality and Service
            </Text.H5>
          </Flex>

          <DivWrapper className="absolute" bottom="15%">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

const BoxContent = ({ data }) => {
  const [hover, setHover] = useState(false);
  return (
    <Flex
      className="responsive-full-width responsive-height250px filter"
      bg={data && `${process.env.api_url}` + data.image[0].url}
      flexDirection="column"
      width="25%"
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H4
        fontColor="white"
        textAlign="center"
        weight={hover === true ? 500 : 100}
      >
        {data.name}
      </Text.H4>
      <Link
        href="/th/service/[...project]"
        as={`/th/service/${data.type}/${data.url}`}
      >
        <a>
          <Button
            margin="0.5em 0"
            bgcolor="red"
            className={
              hover === true ? "inline visible-show" : "inline visible-hide"
            }
          >
            View now
          </Button>
        </a>
      </Link>
    </Flex>
  );
};
export default ServiceType;
