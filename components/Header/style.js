import styled from "styled-components";

export const Wrapper = styled.div`
  background-color: rgb(255, 255, 255, 0.75);
  display: flex;
  justify-content: center;
  border-radius: 0 0 25px 25px;
  -webkit-box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  &.logo {
    width: 250px;
  }
  &.nav {
    height: 60px;
    padding-top: 5px;
  }
`;
export const A = styled.a`
  margin: 1.5em 2.5em;
  font-size: 13px;
  font-weight: 200;
  cursor: pointer;
  color: white;
  text-shadow: 2px 2px 4px #000000;
  &.active {
    color: ${(props) => props.theme.color.red};
    font-weight: 500;
    font-size: 18px;
  }
  &.responsive {
    font-size: 30px;
    margin: 1em 1em;
  }
  &.responsive-active {
    color: ${(props) => props.theme.color.red};
    font-weight: 500;
    font-size: 30px;
    margin-left: 0.5em;
  }
  :hover {
    color: ${(props) => props.theme.color.red};
    text-decoration: none;
    &.active {
      color: ${(props) => props.theme.color.white};
    }
  }
  transition-duration: 0.25s;
`;

export const NavWrapper = styled.div`
  position: absolute;
  height: 100px;
  width: 100%;
  z-index: 99;
`;

export const HamburgerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 70px;
  width: 33px;
  z-index: 99 !important;
  cursor: pointer;
  display: none;
  @media only screen and (max-width: 768px) {
    display: flex;
  }
`;

export const SpanStyle = styled.span`
  display: block;

  height: 4px;
  cursor: pointer;
  margin-bottom: 5px;
  position: relative;
  z-index: 99;
  border-radius: 3px;
  transform-origin: 0px 0px;
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  background: 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  opacity: 0.55s ease;
  &.one {
    width: 33px;
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(45deg) translate(-2px,0px)"};
    background: ${(props) =>
      props.collapsed
        ? `${props.theme.color.red}`
        : `${props.theme.color.red}`};
    transform-origin: ${(props) => (props.collapsed ? "0% 0%" : "none")};
  }
  &.two {
    width: 15px;
    transform-origin: ${(props) => (props.collapsed ? "0% 100%" : "none")};
    opacity: ${(props) => (props.collapsed ? "1" : "0")};
    background: ${(props) => `${props.theme.color.white}`};
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(0deg) scale(0.2, 0.2)"};
  }
  &.three {
    width: ${(props) => (props.collapsed ? `25px` : `33px`)};
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(-45deg) translate(-5px, 0)"};
    background: ${(props) =>
      props.collapsed
        ? `${props.theme.color.blue}`
        : `${props.theme.color.red}`};
  }
`;

export const SlideDrawerStyled = styled.div`
  height: 100%;
  background: rgb(27, 30, 33, 0.96);
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  top: 0;
  right: 0;
  width: 70%;
  z-index: 90;
  box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
  transform: ${(props) =>
    props.collapsed ? "translateX(100%)" : "translateX(0)"};
  transition: transform 0.5s ease-out;
`;
