import React, { useState, useEffect } from "react";
import Link from "next/link";
import { DivWrapper, Flex, Text } from "components";
import { Container } from "reactstrap";
import {
  Wrapper,
  A,
  NavWrapper,
  SlideDrawerStyled,
  HamburgerWrapper,
  SpanStyle,
} from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faUsers } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
export const Header = () => {
  const router = useRouter();
  const [isCollapsed, setCollapsed] = useState(true);
  useEffect(() => {
    if (!isCollapsed) {
      document.body.classList.add("lock-screen");
    } else {
      document.body.classList.remove("lock-screen");
    }
  }, [isCollapsed]);

  return (
    <NavWrapper className="absolute" height="100px" width="100%">
      <Container>
        <Flex justifyContent="space-between" height="100px" alignItems="center">
          <img src="/assets/logo.png" style={{ height: "70px" }} />
          <Flex className="responsive-hide " alignItems="center">
            <Link href="/th/home">
              <A className={router.route === "/th/home" && "active"}>หน้าแรก</A>
            </Link>
            <Link href="/th/aboutus">
              <A className={router.route === "/th/aboutus" && "active"}>
                เกี่ยวกับเรา
              </A>
            </Link>
            <Link href="/th/service">
              <A className={router.route.includes("service") && "active"}>
                บริการ
              </A>
            </Link>
            <Link href="/th/content">
              <A className={router.route.includes("content") && "active"}>
                ข่าวสาร
              </A>
            </Link>
          </Flex>
          <Hamburger isCollapsed={isCollapsed} setCollapsed={setCollapsed} />
          <SlideDrawerStyled collapsed={isCollapsed}>
            <Flex
              flexDirection="column"
              alignItems="flex-start"
              justifyContent="flex-start"
              overflow="scroll"
              margin="2em 0 0 0"
              padding="2em 2em"
            >
              <Link href="/th/home">
                <A
                  className={
                    router.route === "/th/home"
                      ? "responsive-active "
                      : "responsive"
                  }
                  onClick={() => setCollapsed(!isCollapsed)}
                >
                  หน้าแรก
                </A>
              </Link>
              <Link href="/th/aboutus">
                <A
                  className={
                    router.route === "/th/aboutus"
                      ? "responsive-active "
                      : "responsive"
                  }
                  onClick={() => setCollapsed(!isCollapsed)}
                >
                  เกี่ยวกับเรา
                </A>
              </Link>
              <Link href="/th/service">
                <A
                  className={
                    router.route.includes("service")
                      ? "responsive-active "
                      : "responsive"
                  }
                  onClick={() => setCollapsed(!isCollapsed)}
                >
                  บริการ
                </A>
              </Link>
              <Link href="/th/content">
                <A
                  className={
                    router.route.includes("content")
                      ? "responsive-active "
                      : "responsive"
                  }
                  onClick={() => setCollapsed(!isCollapsed)}
                >
                  ข่าวสาร
                </A>
              </Link>

              <img
                src="/assets/logo.png"
                style={{ height: "70px", marginTop: "5em" }}
              />
              <Text.H6 margin="2em 0" fontColor="white">
                Civil Master Decor Co.,Ltd
                <br /> <br />
                96 ซอยรัชดาภิเษก 19, แขวงวัดท่าพระ, เขตบางกอกใหญ่, กรุงเทพ 10600
              </Text.H6>
              <DivWrapper
                margin="2em 0"
                onClick={() =>
                  window.open("http://civilmasterdecor.co.th:1337/", "_blank")
                }
                cursor="pointer"
              >
                <Text.H6 fontColor="blue">LOG IN AS AN ADMIN</Text.H6>
              </DivWrapper>
            </Flex>
          </SlideDrawerStyled>
        </Flex>
      </Container>
    </NavWrapper>
  );
};

const Hamburger = (props) => {
  const { setCollapsed, isCollapsed, isHome } = props;

  return (
    <HamburgerWrapper onClick={() => setCollapsed(!isCollapsed)}>
      <SpanStyle className="one" collapsed={isCollapsed}></SpanStyle>
      <SpanStyle collapsed={isCollapsed} className="two"></SpanStyle>
      <SpanStyle className="three" collapsed={isCollapsed}></SpanStyle>
    </HamburgerWrapper>
  );
};
