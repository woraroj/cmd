import React from "react";
import { SectionStyle } from "./style";
export const Section = (props) => {
  const {
    children,
    className,
    width,
    height,
    bgcolor,
    bg,
    padding,
    minHeight,
  } = props;
  return (
    <SectionStyle
      className={className}
      width={width}
      height={height}
      bgcolor={bgcolor}
      padding={padding}
      bg={bg}
      minHeight={minHeight}
    >
      {children}
    </SectionStyle>
  );
};
