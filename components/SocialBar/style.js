import styled from "styled-components";

export const Text = styled.p`
  color: rgb(255, 255, 255, 0.8);
  font-weight: 300;
  font-size: 18px;
  letter-spacing: 1px;
  margin: 0 0.5em 0 0;
`;

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-size: 15px;
  width: 35px;
  height: 35px;
  margin-left: 0.5em;
  cursor: pointer;
  transition-duration: 0.3s;
  :hover {
    color: red;
    &.white {
      color: white;
    }
  }
`;
