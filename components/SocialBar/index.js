import React from "react";
import { Flex } from "components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faInstagram,
  faYoutube,
  faLine,
} from "@fortawesome/free-brands-svg-icons";
import { Text, LogoWrapper } from "./style";
export const SocialBar = ({ className }) => {
  const handleClick = (url) => {
    window.open(url, "_blank");
  };
  return (
    <Flex alignItems="center">
      <Text>visit us on</Text>
      <LogoWrapper
        className={className}
        onClick={() =>
          handleClick("https://www.facebook.com/CivilMasterDecor1995")
        }
      >
        <FontAwesomeIcon icon={faFacebookSquare} style={{ width: "20px" }} />
      </LogoWrapper>
      {/* <LogoWrapper
        className={className}
        onClick={() => handleClick("https://www.instagram.com/")}
      >
        <FontAwesomeIcon icon={faInstagram} style={{ width: "20px" }} />
      </LogoWrapper> */}
      <LogoWrapper
        className={className}
        onClick={() => handleClick("https://lin.ee/omULbgD")}
      >
        <FontAwesomeIcon icon={faLine} style={{ width: "20px" }} />
      </LogoWrapper>
      {/* <LogoWrapper
        className={className}
        onClick={() => handleClick("https://www.youtube.com/")}
      >
        <FontAwesomeIcon icon={faYoutube} style={{ width: "25px" }} />
      </LogoWrapper> */}
    </Flex>
  );
};
