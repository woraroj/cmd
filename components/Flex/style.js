import styled from "styled-components";

export const FlexStyle = styled.div`
  position: relative;
  display: flex;
  background-image: url(${(props) => props.bg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  flex-direction: ${(props) => props.flexDirection};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alignItems};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  min-height: ${(props) => props.minHeight};
  flex-wrap: ${(props) => props.wrap};
  cursor: ${(props) => props.cursor};
  filter: ${(props) => props.filter};
  overflow: ${(props) => props.overflow};
  &.filter {
    background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
      url(${(props) => props.bg});
  }
  &.shadow {
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
  }

  :hover {
    &.hexagon {
      background-color: #0a72b6;
    }
    &.addBgfilter {
      background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
        url(${(props) => props.bg});
    }
  }
  @media only screen and (max-width: 768px) {
    &.responsive-hide {
      display: none;
    }
    &.responsive-full-width {
      width: 100%;
    }
    &.responsive-justify-center {
      justify-content: center;
    }
    &.responsive-align-center {
      align-items: center;
    }
    &.responsive-column {
      flex-direction: column;
    }
    &.responsive-order-0 {
      order: 0;
    }
    &.responsive-order-1 {
      order: 1;
    }
    &.responsive-height250px {
      height: 250px;
    }
  }
`;
