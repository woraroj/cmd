import React from "react";
import { FlexStyle } from "./style";

export const Flex = (props) => {
  const {
    children,
    className,
    flexDirection,
    justifyContent,
    alignItems,
    margin,
    padding,
    width,
    height,
    minHeight,
    wrap,
    bg,
    cursor,
    onMouseOver,
    onMouseLeave,
    filter,
    overflow,
  } = props;
  return (
    <FlexStyle
      className={className}
      flexDirection={flexDirection}
      justifyContent={justifyContent}
      alignItems={alignItems}
      margin={margin}
      padding={padding}
      width={width}
      height={height}
      minHeight={minHeight}
      wrap={wrap}
      bg={bg}
      cursor={cursor}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
      filter={filter}
      overflow={overflow}
    >
      {children}
    </FlexStyle>
  );
};
