import React from "react";
import Link from "next/link";
import { Section, Flex, Text, SocialBar } from "components";
import { Container, Badge } from "reactstrap";
import { A } from "./style";
export const Footer = () => {
  return (
    <Section bgcolor="red" padding="2em 0">
      <Container>
        <Flex justifyContent="space-between">
          <Flex flexDirection="column" alignItems="flex-start">
            <img src="/assets/logo.png" style={{ height: "70px" }} />
            <Text.H5
              fontColor="white"
              lineHeight="25px"
              weight={200}
              margin="1em 0"
            >
              96 Ratchadaphisek 19 Alley,
              <br /> Wat Tapra, Bangkok Yai,
              <br /> Bangkok 10600
            </Text.H5>
          </Flex>
          <Flex justifyContent="flex-start" alignItems="flex-start">
            <Flex flexDirection="column">
              <Flex>
                <Link href="/th/home">
                  <A>หน้าแรก</A>
                </Link>
                <Link href="/th/aboutus">
                  <A>เกี่ยวกับเรา</A>
                </Link>
                <Link href="/th/service">
                  <A>บริการ</A>
                </Link>
                <Link href="/th/content">
                  <A>ข่าวสาร</A>
                </Link>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
        <Flex
          className="responsive-column "
          alignItems="center"
          justifyContent="space-between"
        >
          <Text.H5 fontColor="white" weight={200}>
            Copyright©2021, Civil Master Decor Co., Ltd. All rights reserved.
          </Text.H5>
          <SocialBar className="white" />
        </Flex>
        <Flex
          className="responsive-column responsive-align-center"
          alignItems="center"
          justifyContent="space-between"
        >
          <Text.H6
            fontColor="white"
            margin="1em 0"
            weight={200}
            lineHeight="25px"
          >
            Design and Develop by
            <a target="_blank" href="https://worarojs.github.io/myPort/">
              <Badge
                className="p-2 m-1"
                style={{ cursor: "woraroj " }}
                color="light"
              >
                Chin Su
              </Badge>
            </a>
            associated with{" "}
            <a target="_blank" href="http://aitechnovation.com/">
              AI Technovation Co.,Ltd.
            </a>
          </Text.H6>
          <A target="_blank" href="http://civilmasterdecor.co.th:1337/">
            LOG IN AS AN ADMIN
          </A>
        </Flex>
      </Container>
    </Section>
  );
};
