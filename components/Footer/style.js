import styled from "styled-components";

export const A = styled.a`
  margin: 1.5em 2.5em;
  font-size: 15px;
  font-weight: 400;
  cursor: pointer;
  color: white;

  :hover {
    color: ${(props) => props.theme.color.white};
    font-size: 16px;
    text-decoration: none;
  }
  transition-duration: 0.25s;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;
