import styled from "styled-components";

export const DivStyle = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  padding: ${(props) => props.padding};
  margin: ${(props) => props.margin};
  cursor: ${(props) => props.cursor};
  &.client-image {
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    width: 100%;
    max-height: 480px;
    height: 100%;
    transition-duration: 0.5s;
    cursor: grab;
  }
  &.absolute {
    position: absolute;
    top: ${(props) => props.top};
    left: ${(props) => props.left};
    bottom: ${(props) => props.bottom};
    right: ${(props) => props.right};
  }
`;
