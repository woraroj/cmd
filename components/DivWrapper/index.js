import React from "react";
import { DivStyle } from "./style";

export const DivWrapper = (props) => {
  const {
    children,
    className,
    width,
    height,
    padding,
    margin,
    top,
    left,
    bottom,
    right,
    onClick,
    cursor,
  } = props;
  return (
    <DivStyle
      className={className}
      width={width}
      height={height}
      padding={padding}
      margin={margin}
      top={top}
      left={left}
      bottom={bottom}
      right={right}
      onClick={onClick}
      cursor={cursor}
    >
      {children}
    </DivStyle>
  );
};
