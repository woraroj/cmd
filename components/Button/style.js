import { Button } from "reactstrap";
import styled from "styled-components";

export const ButtonStyle = styled(Button)`
  background-color: ${(props) => props.theme.color[props.bgcolor]};
  border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
  border-radius: 5px;
  font-weight: 200;
  padding: 0.3em 1.5em;
  margin: ${(props) => props.margin};

  transition-duration: 0.15s;

  &.inline {
    background-color: rgb(0, 0, 0, 0);
    border: 1px solid ${(props) => props.theme.color[props.bgcolor]};
    color: ${(props) => props.theme.color[props.bgcolor]};
  }
  &.visible-show {
    opacity: 1;
    visibility: visible;
    transition: visibility 0.3s linear, opacity 0.3s linear, margin-bottom 0.3s;
  }
  &.visible-hide {
    opacity: 0;
    visibility: hidden;
    transition: visibility 0.3s linear, opacity 0.3s linear, margin-bottom 0.3s;
    margin-bottom: -45px;
  }
  :hover {
    background-color: rgb(0, 0, 0, 0);
    color: ${(props) => props.theme.color[props.bgcolor]};
    border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
    font-weight: 500;

    &.inline {
      background-color: ${(props) => props.theme.color[props.bgcolor]};
      border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
      color: rgb(255, 255, 255);
    }
  }
`;
