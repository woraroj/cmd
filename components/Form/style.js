import styled from "styled-components";
import { Field, Form } from "formik";

export const TextField = styled(Field)`
  background: rgb(0, 0, 0, 0);
  border: 0;
  border-radius: 0;
  border-bottom: 2px solid red;
  margin: ${(props) => props.margin};
  color: rgb(255, 255, 255);
  height: ${(props) => props.height};
  width: 100%;
  font-size: 1em;
  padding: 5px 10px;
  font-weight: 400;

  ::placeholder {
    color: rgb(255, 255, 255, 0.8);
    font-weight: 200;
  }
`;

export const FormikForm = styled(Form)`
  width: 100%;
`;

export const Checkbox = styled(Field)``;
